package pacote.Lavajet;

import java.util.Scanner;

import pacote.Lavajet.classes.Clientes;
import pacote.Lavajet.classes.Servi�os;
import pacote.Lavajet.classes.Veiculo;

public class Execu��oSistema {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws InterruptedException {
		
		Scanner sc = new Scanner(System.in);
		
		//Instanciar as classes criadas ( atributos e metodos )
		Clientes cliente = new Clientes();
		Veiculo veiculo = new Veiculo();
		Servi�os servico = new Servi�os();
			
		System.out.println("----------- Bem vindo ao sistema LavaJet ---------------- ");
		System.out.println("==========================================================");
		
			while (true){
			
			System.out.println("==========================================================");			
			System.out.println("\n Seleciona uma das op��es abaixo:\n ");
			System.out.println(" => Op��o 1: Clientes <= ");
			System.out.println(" => Op��o 2: Veiculo <= ");
			System.out.println(" => Op��o 3: Servi�os <= ");
			System.out.println(" => Op��o 4: Sair <= ");
			System.out.println("\n==========================================================");
			
			Thread.sleep(1 * 1000);
			
			int opcao = sc.nextInt();
			
			
			if (opcao == 4) {
				
				System.out.println("At� logo !  ");
				break;
				
			}
						
			switch (opcao) {
									
			case 1:
				System.out.println("==========================================================");
				System.out.println(" => Op��o 1 selecionada , clientes <= ");
				System.out.println("==========================================================");
				
							
				System.out.println("\nInforme o codigo do cliente:");
				cliente.setCodigoCliente(sc.nextInt());
							
				//Ap�s o uso do nextInt, caso o scanner pule uma linha ... adiciona logo abaixo o um nextLine.
				sc.nextLine();
				
				
				System.out.println("Digite o nome do cliente: ");
				cliente.setNome(sc.nextLine());
								
				System.out.println("Por favor nos informe o cpf do cliente: ");
				cliente.setCpf(sc.nextLine());
				
				System.out.println("Digite o endere�o do cliente: ");
				cliente.setEndereco(sc.nextLine());
				
				System.out.println("Digite o telefone do cliente: ");
				cliente.setTelefone(sc.nextLine());
				
				System.out.println("Digite o e-mail do cliente: ");
				cliente.setEmail(sc.nextLine());
				
				System.out.println("Realizando o cadastro 1 momnento.... ");
				
				Thread.sleep(2 * 1000);
				
				System.out.println("\n---------------------------------------");
				
				System.out.println("O codigo do cliente �: " + cliente.getCodigoCliente());
				System.out.println("O nome do cliente �: " + cliente.getNome());
				System.out.println("O cpf do cliente �: " + cliente.getCpf());
				System.out.println("O endere�o do cliente �: " + cliente.getEndereco());
				System.out.println("O telefone do cliente �: " + cliente.getTelefone());
				System.out.println("O e-mail do cliente �: " + cliente.getEmail());
				
				System.out.println("\n---------------------------------------");
				
				break;
				
			case 2:
				
				System.out.println("==========================================================");
				System.out.println(" => Op��o 2 selecionada , Veiculos <= ");
				System.out.println("==========================================================");
			
				System.out.println("Informe o codigo do veiculo: ");
				veiculo.setCodigoDoVeiculo(sc.nextInt());
				
				sc.nextLine();
				
				System.out.println("Informe o modelo do veiculo: ");
				veiculo.setModelo(sc.nextLine());
				
				System.out.println("Informe a marca do veiculo: ");
				veiculo.setMarca(sc.nextLine());
				
				System.out.println("Informe a placa do veiculo: ");
				veiculo.setPlaca(sc.nextLine());
				
				System.out.println("Realizando o cadastro 1 momnento.... ");
				
				Thread.sleep(2 * 1000);
				
				System.out.println("\n---------------------------------------");
				
				System.out.println("O codigo do veiculo : " + veiculo.getCodigoDoVeiculo());
				System.out.println("O modelo informado �: " + veiculo.getModelo());
				System.out.println("A marca informada : " + veiculo.getMarca());
				System.out.println("A placa informada � : " + veiculo.getPlaca());
								
				System.out.println("\n---------------------------------------");
				
				break;
										
			case 3:
				
				System.out.println("==========================================================");
				System.out.println(" => Op��o 3 selecionada , Servi�os <= ");
				System.out.println("==========================================================");
				
				
				System.out.println("Informe o codigo do servi�o: ");
				servico.setCodigoServi�o(sc.nextInt());
				
				sc.nextLine();
				
				System.out.println("Informe o tipo de servi�o: ");
				servico.setTipoDeServi�o(sc.nextLine());
				
				System.out.println("Informe o agendamento: ");
				servico.setAgendamento(sc.nextLine());
				
				System.out.println("Informe o tipo de pagamento: ");
				servico.setTipoDePagamento(sc.nextLine());
				
				
				System.out.println("Informe o valor do servi�o: ");
				servico.setValores(sc.nextFloat());
				
				
				System.out.println("Realizando o cadastro 1 momnento.... ");
				
				Thread.sleep(2 * 1000);
				
				System.out.println("\n---------------------------------------");
				
				System.out.println("O codigo do servi�o : " + servico.getCodigoServi�o());
				System.out.println("O tipo do servi�o �: " + servico.getTipoDeServi�o());
				System.out.println("O agendamento ficou no dia: " + servico.getAgendamento());
				System.out.println("O tipo de pagamento foi: " + servico.getTipoDeServi�o());
				System.out.println("O Valor do servi�o foi: " + servico.getValores() + " R$");
				
								
				System.out.println("\n---------------------------------------");
				
				
				break;
				
			default:
				
				System.out.println("\n-------------------------------------------------------------------------");
				System.out.println("\n Voc� digitou um comando invalido, por favor selecione uma op��o correta.");
				System.out.println("\n-------------------------------------------------------------------------");
				
				Thread.sleep(3 * 1000);
				
			}
			
						
			
		}
		
		

		
		
		

}
}
	
