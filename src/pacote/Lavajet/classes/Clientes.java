package pacote.Lavajet.classes;

public class Clientes {

	public int codigoCliente;
	public String nome , cpf , endere�o , telefone , email;
	
		
	public void setCodigoCliente(Integer codigoCliente) {
		
		this.codigoCliente = codigoCliente;
				
	}	
	
	public Integer getCodigoCliente() {
		
		return codigoCliente;
						
	}
	
	public void setNome(String nome) {
		
		this.nome = nome;
		
	}
	
	public String getNome() {
		
		return nome;
		
	}
	
	public void setCpf(String cpf) {
		
		this.cpf = cpf;
		
	}

	public String getCpf() {
		
		return cpf;
		
	}
		
	public void setEndereco(String endere�o) {
		
		this.endere�o = endere�o;
		
	}

	public String getEndereco() {
		
		return endere�o;
		
	}

	public void setTelefone(String telefone) {
		
		this.telefone = telefone;
		
	}

	public String getTelefone() {
		
		return telefone;
	}
		
	public void setEmail(String email) {
		
		this.email = email;
		
	}
	
	public String getEmail() {
		
		return email;
	}


}
