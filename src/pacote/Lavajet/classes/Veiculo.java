package pacote.Lavajet.classes;

public class Veiculo {

	//codigo do carro, modelo, marca , placa , // codigo do cliente ( tem que puxar da classe cliente ) , historico do carro ( tem que puxar da classe historico
	
	public int codigoDoVeiculo;
	public String modelo, marca, placa;
	
		
	public void setCodigoDoVeiculo(Integer codigoDoVeiculo ) {
		
		this.codigoDoVeiculo = codigoDoVeiculo;
		
	}
	
	public int getCodigoDoVeiculo() {
		
		return codigoDoVeiculo; 
	}

	public void setModelo(String modelo) {
		
		this.modelo = modelo;
		
	}

	public String getModelo() {

		
		return modelo;
	}

	public void setMarca (String marca) {
		
		this.marca = marca;
		
	}
	
	public String getMarca() {
	
		return marca;
}
	
	public void setPlaca (String placa) {
		
		this.placa = placa;
	}
	
	public String getPlaca() {
		
		return placa;
	}
	
	
	
	
	
}
