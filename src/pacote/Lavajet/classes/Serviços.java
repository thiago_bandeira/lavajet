package pacote.Lavajet.classes;

public class Serviços {
	
	//codigo do serviço,  tipo do serviço, agendamento, valores, tipo de pagamento , // tem que puxar da classe carro codigo do carro
	
	public int codigoServiço;
	public String tipoDeServiço, agendamento , tipoDePagamento;
	public float valores;
	
	
	
	public void setCodigoServiço(Integer codigoServiço) {
		
		this.codigoServiço = codigoServiço;
	}
	
	public Integer getCodigoServiço() {
		
		return codigoServiço;
		
	}

	
	public void setTipoDeServiço (String tipoDeServiço) {
		
		this.tipoDeServiço = tipoDeServiço;
		
	}
	
	public String getTipoDeServiço() {
		
		return tipoDeServiço;
	}

	
	public void setAgendamento (String agendamento) {
		
		this.agendamento = agendamento;
		
	}

	public String getAgendamento () {
		
		return agendamento;
	}


	public void setValores(float valores) {
		
		this.valores = valores;
	}
	
	public Float getValores() {
		
		return valores;
	}
	
	
	
	public void setTipoDePagamento(String tipoDePagamento) {
		
		this.tipoDePagamento = tipoDePagamento;
	}

	public String getTipoDePagamento() {
		
		return tipoDePagamento;
	}

	





}
